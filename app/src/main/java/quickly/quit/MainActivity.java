package quickly.quit;

import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.os.Build;
import android.view.WindowManager;
import android.content.ComponentName;
import android.content.Intent;
import android.app.admin.DevicePolicyManager;
import android.preference.PreferenceManager;
import android.preference.Preference;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Context;
import android.widget.Toast;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.preference.CheckBoxPreference;
import android.provider.Settings;
import android.net.Uri;
import android.app.Activity;

public class MainActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int display = Integer.parseInt(preferences.getString("pref_display", "0"));

        //check for admin permission to be able to lock screen
        //is not, ask for permissions
        //else display notification or floating button
        if (!deviceAdminGranted())
            requestDeviceAdmin();
        else {
            if (display == 0) {
                displayNotification();
            } else if (display == 1) {
                displayFloatingButton();
            } else {
                displayNotification();
                displayFloatingButton();
            }
        }

        //Uninstall the app
        Preference uninstall = findPreference("pref_uninstall");
        uninstall.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {   
                if (deviceAdminGranted()) {
                    removeDeviceAdmin();
                }
                Intent iu = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivityForResult(iu, 1989);
                return true;
            }
        });
    }

    public boolean deviceAdminGranted() {
        ComponentName cn = new ComponentName(MainActivity.this, LockScreenDeviceAdminReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm.isAdminActive(cn);
    }

    public void displayNotification() {
        //only display if permission is granted
        if (deviceAdminGranted()) {
            Intent dnsi = new Intent(MainActivity.this, DisplayNotificationService.class);
            dnsi.putExtra("origin", "main");
            MainActivity.this.startService(dnsi);
        }
    }

    public void displayFloatingButton() {
        //only display if permission is granted
        if (deviceAdminGranted()) {
            Intent fbsi = new Intent(MainActivity.this, FloatingButtonService.class);
            MainActivity.this.startService(fbsi);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                Intent hai = new Intent(MainActivity.this, HelpActivity.class);
                MainActivity.this.startActivity(hai);
                return true;
            case R.id.about:
                Intent aai = new Intent(MainActivity.this, AboutActivity.class);
                MainActivity.this.startActivity(aai);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if device admin activated
        if(requestCode == 1922 && resultCode == Activity.RESULT_OK) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            int display = Integer.parseInt(preferences.getString("pref_display", "0"));
            if (display == 0) {
                displayNotification();
            } else if (display == 1) {
                displayFloatingButton();
            } else {
                displayNotification();
                displayFloatingButton();
            }
        }

        //if overlay permission not granted, show a toast and set pref_display to 0 (only notification)
        if (requestCode == 1954 && !Settings.canDrawOverlays(this)) {
            Toast.makeText(MainActivity.this, getString(R.string.no_overlay_permission_given), Toast.LENGTH_LONG).show();
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("pref_display", "0").commit();
            setPreferenceScreen(null);
            addPreferencesFromResource(R.xml.preferences);
        }

        //if overlay and device admin permissions granted, display the floating button
        if (requestCode == 1954 && Settings.canDrawOverlays(this) && deviceAdminGranted()) {
            displayFloatingButton();
        }

        //if not uninstalled, request device admin again
        if (requestCode == 1989) {
            requestDeviceAdmin();
        }
    }

    public void requestDeviceAdmin() {
        ComponentName cn = new ComponentName(MainActivity.this, LockScreenDeviceAdminReceiver.class);
        Intent dai = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        dai.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, cn);
        dai.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getString(R.string.admin_explanation));
        startActivityForResult(dai, 1922);
    }

    public void removeDeviceAdmin() {
        ComponentName cn = new ComponentName(MainActivity.this, LockScreenDeviceAdminReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        dpm.removeActiveAdmin(cn);
        //remove the notification
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(1922);
        //remove the floating button
        Intent fbsi = new Intent(MainActivity.this, FloatingButtonService.class);
        MainActivity.this.stopService(fbsi);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //check device admin when changing preferences too
        if (!deviceAdminGranted()) {
            requestDeviceAdmin();
        }

        if (!deviceAdminGranted() && sharedPreferences.getBoolean("pref_start_on_boot", false)) {
            Toast.makeText(MainActivity.this, getString(R.string.no_permission_given), Toast.LENGTH_SHORT).show();
            CheckBoxPreference check = (CheckBoxPreference) findPreference("pref_start_on_boot");
            check.setChecked(false);
        }

        // if show floating button selected, ask for overlay permission
        if (key.compareTo("pref_display") == 0) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            int display = Integer.parseInt(preferences.getString("pref_display", "0"));
            
            if (display == 0) { // 0 = notification
                displayNotification();
                //remove the floating button
                Intent fbsi = new Intent(MainActivity.this, FloatingButtonService.class);
                MainActivity.this.stopService(fbsi);
            } else if (display == 1) { // 1 = floating button
                //check permission only in android api >= 23
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1954);
                    } else {
                        displayFloatingButton();
                    }
                } else {
                    displayFloatingButton();
                }
                //remove the notification
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancel(1922);
            } else if (display == 2) { // 2 = both
                //check permission only in android api >= 23
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1954);
                    } else {
                        displayFloatingButton();
                    }
                } else {
                    displayFloatingButton();
                }
                displayNotification();
            }
        }
    }
}

package quickly.quit;

import android.app.IntentService;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.app.PendingIntent;
import android.app.Notification;
import android.os.Build;
import android.app.NotificationManager;

public class DisplayNotificationService extends IntentService {

    public DisplayNotificationService() {
        super("DisplayNotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String origin = intent.getExtras().getString("origin");
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean startOnBoot = preferences.getBoolean("pref_start_on_boot", false);

        if (origin.compareToIgnoreCase("boot") == 0 && !startOnBoot) {
            //do nothing
        } else {
            Intent i = new Intent(DisplayNotificationService.this, QuicklyQuitService.class);
            PendingIntent pi = PendingIntent.getService(DisplayNotificationService.this, 0, i, 0);

            Notification.Builder b = new Notification.Builder(this);
            b.setContentTitle(getResources().getString(R.string.app_name));
            b.setContentText(getResources().getString(R.string.tap_to_exit));
            b.setSmallIcon(R.mipmap.ic_launcher);
            b.setContentIntent(pi);
            //setShowWhen: added in API level 17
            if (Build.VERSION.SDK_INT >= 17)
                b.setShowWhen(false);
            //setPriority: added in API level 16, deprecated in API level 26
            if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 26)
                b.setPriority(Notification.PRIORITY_HIGH);
            //setVisibility: added in API level 21
            if (Build.VERSION.SDK_INT >= 21)
                b.setVisibility(Notification.VISIBILITY_SECRET);

            //
            int display = Integer.parseInt(preferences.getString("pref_display", "0"));
            if (display == 2) {
                Intent fbsi = new Intent(DisplayNotificationService.this, FloatingButtonService.class);
                PendingIntent pfbsi = PendingIntent.getService(DisplayNotificationService.this, 0, fbsi, 0);
                b.addAction(R.mipmap.ic_launcher, getResources().getString(R.string.action_display_button_again), pfbsi);
            }
            //

            Notification n;
            //build: added in API level 16
            if (Build.VERSION.SDK_INT >= 16)
                n = b.build();
            else
                n = b.getNotification();

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.notify(1922, n);
        }
    }
}

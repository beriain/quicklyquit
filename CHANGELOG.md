# 1.2.3
- Add an action to notification to display the button again
- Update launcher and floating button icons

# 1.2.2
- Set "Start on boot" preference as enabled by default
- Add an option to not mute when headphones are connected
- Add a note about battery optimization and floating button
- Make the revoke admin button the uninstall button

# 1.2.1
- Move tips to a proper help activity
- Add an option to remove the notification and/or the floating button after use
- Add an option to avoid going to the home screen when locking the device

# 1.2
- Add a floating button to quickly quit

# 1.1
- Add an option to mute multimedia when locking the screen
- Add a confirmation toast when deactivating device admin

# 1.0.1
- Fix a NoSuchMethodError in API 15
- Add screenshots

# 1.0
- Initial release
* Move tips to a proper help activity
* Add an option to remove the notification and/or the floating button after use
* Add an option to avoid going to the home screen when locking the device
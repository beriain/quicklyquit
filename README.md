# <img src="https://framagit.org/uploads/-/system/project/avatar/32207/ic_launcher.png?width=22" alt="" height="40"> Quickly quit

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/quickly.quit)

Quickly quit what you where doing

Go to the home screen and lock the device from the notification area or a floating button.

You can also mute your device when quickly quitting. 

This app can be built using [Debian Android SDK](https://wiki.debian.org/AndroidTools)